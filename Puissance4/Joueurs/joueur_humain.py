#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    game.affiche(jeu)
    joueur=jeu[1]
    liste=game.getCoupsValides(jeu)
    if liste==[] :
    	return []
    print(liste)
    c=input("Choisissez le numero du coup que vous souhaitez jouer dans la liste ci-dessus :" )
    while (c not in ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15']):
    	c=input("Coup Invalide !\nRechoisissez :")	
    c=int(c)
    coup=liste[c]
    return coup
