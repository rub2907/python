#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game

global hor
hor=3

def evaluation (jeu) :
    score=game.getScores(jeu)
    return (score[moi-1]-score[(moi%2+1)-1])

def estimation (jeu,coup,horizon,alpha,beta) :
    jeu1=game.getCopieJeu(jeu)
    game.joueCoup(jeu1,coup)
    #print(horizon)
    if game.finJeu(jeu1):
        if game.getGagnant(jeu1)==moi:
            return 1000
        else : 
            return -1000

    elif (horizon==hor) :
        #print("ici",res)
        return evaluation(jeu1)
    else:
        ListCV=game.getCoupsValides(jeu1)
        if jeu1[1]==moi:
            s=-100000
            for i in ListCV :
                s=max(s,estimation(jeu1,i,horizon+1,alpha,beta))
                if s>=beta :
                    return beta+1
                else :
                    alpha=max(alpha,s)
            return s

        else:
            s=100000
            for i in ListCV :
                s=min(s,estimation(jeu1,i,horizon+1,alpha,beta))
                if s<=alpha :
                    return alpha-1
                else :
                    beta=min(beta,s)
            return s
            

#dans un noeud min, on met à jour beta et on coupe l'evaluation en fonction de alpha (si une valeur des enfants est inférieure à alpha)
#dans un noeud max, on met à jour alpha et on  coupe l'evaluation en fonction de beta (si une valeur des enfants est supérieure à beta) 
        
def decision(jeu,ListCV):
    
    score=jeu[4]
    #joueur=jeu[1]
    res=[]
    alpha=-100000
    for i in ListCV :
        a=estimation(jeu,i,1,alpha,100000)
        #print(a)
        #print(jeu[1])
        if a>alpha :
            alpha=a
            res=i
    return res






def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    liste=game.getCoupsValides(jeu)
    global moi
    moi=jeu[1]
    plateau=jeu[0]
    if plateau[0][3]==0 :
        return [0,3]
    game.affiche(jeu)
    return decision(jeu,liste)