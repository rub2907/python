#!/usr/bin/env python
# -*- coding: utf-8 -*-

def InitialiseJeu():
	Plateau=[[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]]
	List_Score=[0,0]
	joueur=1
	List_coupsV=None
	List_coups=[]
	Jeu=[Plateau,joueur,List_coupsV,List_coups ,List_Score]
	return Jeu

def tiret():
    print("\n-------------------------------------------------------------------")


def coups(jeu):
    """jeu->List[coup]
        Return une liste de coups pour le jeu tels qu'ils concernent chacun une case vide
    """
    plateau=jeu[0]
    L=[]
    for i in range(6):
        for j in range(7):
            if plateau[i][j]==0 :
                coup=[i,j]
                L.append(coup)
    return L

def listeCoupsValides(jeu):
	plateau=jeu[0]
	L=[]
	i=0
	j=0
	while j<7:
		i=0
		while i<6:
			if plateau[i][j]==0 :
				L.append([i,j])
				j+=1
				i=0
				break
			else :
				i+=1
	return L

def suite (jeu) :
	plateau=jeu[0]
	score=jeu[4]
	joueur=jeu[1]
	j=0
	for i in range (6):
		for j in range (4) :
			if plateau[i][j]!=0 and plateau[i][j]==plateau[i][j+1] and plateau[i][j+1]==plateau[i][j+2] and plateau[i][j]==plateau[i][j+3] :
				
				return plateau[i][j]
	for j in range (7) :
		for i in range(3) :
			if plateau[i][j]!=0 and plateau[i][j]==plateau[i+1][j] and plateau[i][j]==plateau[i+2][j] and plateau[i][j]==plateau[i+3][j] :
								return plateau[i][j]
	for i in range (3) :
		for j in range (4):
			if plateau[i][j]!=0 and plateau[i][j]==plateau[i+1][j+1] and plateau[i][j]==plateau[i+2][j+2] and plateau[i][j]==plateau[i+3][j+3] :
				
				return plateau[i][j]

			
	for i in range (3) :
		for j in range (3,7):
			if plateau[i][j]!=0 and plateau[i][j]==plateau[i+1][j-1] and plateau[i][j]==plateau[i+2][j-2] and plateau[i][j]==plateau[i+3][j-3] :
				
				return plateau[i][j]
	return False

def finJeu(jeu):
	if len(listeCoupsValides(jeu))==0 or suite(jeu)!=False :
		return True
	return False

def finalise_partie(jeu):
	score=jeu[4]
	score[jeu[1]-1]=100
	return 
    
