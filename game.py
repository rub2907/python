#!/usr/bin/env python
# -*- coding: utf-8 -*-

# plateau: List[List[nat]]
# liste de listes (lignes du plateau) d'entiers correspondant aux contenus des cases du plateau de jeu

# coup:[nat nat]
# Numero de ligne et numero de colonne de la case correspondante a un coup d'un joueur

# Jeu
# jeu:[plateau nat List[coup] List[coup] List[nat nat]]
# Structure de jeu comportant :
#           - le plateau de jeu
#           - Le joueur a qui c'est le tour de jouer (1 ou 2)
#           - La liste des coups possibles pour le joueur a qui c'est le tour de jouer
#           - La liste des coups joues jusqu'a present dans le jeu
#           - Une paire de scores correspondant au score du joueur 1 et du score du joueur 2

game=None #Contient le module du jeu specifique: awele ou othello
joueur1=None #Contient le module du joueur 1
joueur2=None #Contient le module du joueur 2
import copy
#Fonctions minimales 

def getCopieJeu(jeu):
    """ jeu->jeu
        Retourne une copie du jeu passe en parametre
        Quand on copie un jeu on en calcule forcement les coups valides avant
    """
    
    jeu1=game.InitialiseJeu()
    jeu1[0]=copy.deepcopy(jeu[0])
    jeu1[1]=copy.deepcopy(jeu[1])
    jeu1[2]=copy.deepcopy(jeu[2])
    jeu1[3]=copy.deepcopy(jeu[3])
    jeu1[4]=copy.deepcopy(jeu[4])
    return jeu1

def finJeu(jeu):
    """ jeu -> bool
        Retourne vrai si c'est la fin du jeu
    """
    return game.finJeu(jeu)

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
        On suppose que la fonction n'est appelee que si il y a au moins un coup valide possible
        et qu'elle retourne obligatoirement un coup valide
    """
    if(jeu[1]==1):
        return joueur1.saisieCoup(jeu)


    return joueur2.saisieCoup(jeu)


def getCoupsValides(jeu):
    """ jeu  -> List[coup]
        Retourne la liste des coups valides dans le jeu passe en parametre
        Si None, alors on met � jour la liste des coups valides
    """
    if jeu[2] is None: 
        jeu[2]=game.listeCoupsValides(jeu)
    return jeu[2]

def coupValide(jeu,coup):
	"""jeu*coup->bool
    Retourne vrai si le coup appartient a la liste de coups valides du jeu"""
	if(coup in game.listeCoupsValides(jeu)):
		return True
	return False



def joueCoup(jeu,coup):
    """jeu*coup->void
        Joue un coup a l'aide de la fonction joueCoup defini dans le module game
        Hypothese:le coup est valide
        Met tous les champs de jeu à jour (sauf coups valides qui est fixée à None)
    """
    plateau=jeu[0]
    if len(jeu[0])==2: 
        jeu[3].append(coup)
        if coup==[]:
           #affiche(jeu)
            game.distribue(jeu,coup,0)
    
        else :
            game.distribue(jeu,coup, getCaseVal(jeu, coup[0], coup[1]))
            changeJoueur(jeu)
            jeu[2]=None

    elif len(jeu[0])==8 :
        jeu[3].append(coup)
        if coup==[]:
            game.Score(jeu)
            #affiche(jeu)
        else:
            game.retournePion(jeu,coup)
            game.Score(jeu)
            changeJoueur(jeu)
            jeu[2]=None
            #affiche(jeu)
    elif len(jeu[0])==6 :
        jeu[3].append(coup)
        plateau[coup[0]][coup[1]]=getJoueur(jeu)
        changeJoueur(jeu)
        jeu[2]=None





def initialiseJeu():
    """ void -> jeu
        Initialise le jeu (nouveau plateau, liste des coups joues vide, liste des coups valides None, scores a 0 et joueur = 1)
    """
    jeu=game.InitialiseJeu()
    return jeu

def getGagnant(jeu):
    """jeu->nat
    Retourne le numero du joueur gagnant apres avoir finalise la partie. Retourne 0 si match nul
    """
    changeJoueur(jeu)
    game.finalise_partie(jeu)
    score=jeu[4]
    if score[0]==score[1]:
    	#print("Match NUL !", getScores(jeu))
    	return 0
    elif score[1]==max(score) :
    	#print("Joueur 2 a gagne avec un score de :",getScores(jeu))
    	return 2
    else :
    	#print ("Joueur 1 a gagne avec un score de :",getScores(jeu))
    	return 1

def affiche(jeu):
    """ jeu->void
        Affiche l'etat du jeu de la maniere suivante :
                 Coup joue = <dernier coup>
                 Scores = <score 1>, <score 2>
                 Plateau :

                         |       0     |     1       |      2     |      ...
                    ------------------------------------------------
                      0  | <Case 0,0>  | <Case 0,1>  | <Case 0,2> |      ...
                    ------------------------------------------------
                      1  | <Case 1,0>  | <Case 1,1>  | <Case 1,2> |      ...
                    ------------------------------------------------
                    ...       ...          ...            ...
                 Joueur <joueur>, a vous de jouer
                    
         Hypothese : le contenu de chaque case ne depasse pas 5 caracteres
    """
    if (jeu[3]==[]):
    	print("Aucun coup joué")
    	score=jeu[4] 
    	print("Score :",score[0]," : ",score[1])
    else :
    	taille=len(jeu[3])
    	score=jeu[4]

    	List_coups=jeu[3]

    	dernierC=List_coups[taille-1]
    	print("Coup joue ",dernierC,"\n")
    	print("Score :",score[0]," : ",score[1])
    print("Plateau :")
    plateau=jeu[0]
    nbColonnes=len(plateau[0])
    nbLignes=len(plateau)
    k=0

    for k  in range(nbColonnes) :
    	if (k==0):
    		print("   |   ",k,end="   |")
    	else :
    		print("   ",k,end="   |")
    game.tiret()
    i=0
    for i in range (nbLignes):
    	liste=plateau[i]
    	j=0
    	for j in range(nbColonnes) :
    		if (j==0):
    			print(i," |   ",liste[j],end="   |")
    		else :
    			print("   ",liste[j],end="   |")
    	game.tiret()
    print("Joueur : ",jeu[1]," a vous de jouer")



# Fonctions utiles

def getPlateau(jeu):
    """ jeu  -> plateau
        Retourne le plateau du jeu passe en parametre
    """
    return jeu[0]

def getCoupsJoues(jeu):
    """ jeu  -> List[coup]
        Retourne la liste des coups joues dans le jeu passe en parametre
    """
    return jeu[3]



def getScores(jeu):
    """ jeu  -> Pair[nat nat]
        Retourne les scores du jeu passe en parametre
    """
    return jeu[4]

def getJoueur(jeu):
    """ jeu  -> nat
        Retourne le joueur a qui c'est le tour de jouer dans le jeu passe en parametre
    """
    return jeu[1]



def changeJoueur(jeu):
    """ jeu  -> void
        Change le joueur a qui c'est le tour de jouer dans le jeu passe en parametre (1 ou 2)
    """
    if (jeu[1]==1) :
    	jeu[1]=2
    else :
    	jeu[1]=1

def getScore(jeu,joueur):
    """ jeu*nat->int
        Retourne le score du joueur
        Hypothese: le joueur est 1 ou 2
    """
    score=jeu[4]
    return score[joueur-1]



def getCaseVal(jeu, ligne, colonne):
    """ jeu*nat*nat -> nat
        Retourne le contenu de la case ligne,colonne du jeu
        Hypothese: les numeros de ligne et colonne appartiennent bien au plateau  : ligne<=getNbLignes(jeu) and colonne<=getNbColonnes(jeu)
    """
    plateau=jeu[0]
    colonnes=plateau[ligne]
    res=colonnes[colonne]
    return res   
    
def getNBlignes(jeu):
	"""jeu->nat
		retourne le nombre de ligne du jeu
	"""
	plateau=jeu[0]
	res=len(plateau)
	return res

def getNBcolonnes(jeu):
	"""jeu->nat
		retourne le nombre de colonne du jeu
	"""
	plateau=jeu[0]
	colonne=plateau[0]
	res=len(colonne)
	return res






