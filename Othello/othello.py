#!/usr/bin/env python
# -*- coding: utf-8 -*-

def InitialiseJeu():
	Plateau=[[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,1,2,0,0,0],[0,0,0,2,1,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0]]
	List_Score=[2,2]
	joueur=1
	List_coupsV=None
	List_coups=[]
	Jeu=[Plateau,joueur,List_coupsV,List_coups ,List_Score]
	return Jeu
jeu=InitialiseJeu()

def tiret():
    print("\n----------------------------------------------------------------------------")

def trouveEncadrements(jeu,coup,tous=True):
    """jeu*coup*bool->List[Pair[nat nat]]
        Retourne la liste de directions dans lesquelles on trouve un encadrement pour le coup
        si tous est False, on s'arrete au premier trouve
    """
    joueur=jeu[1]
    Liste=[]
    Plateau=jeu[0]
    Colonne=Plateau[coup[0]]
    i=coup[0]
    j=coup[1]
    caseD=[i,j]

    #EST
    if (coup[1]<6):
        i=coup[0]
        j=coup[1]+1
        k=0
        if Plateau[i][j]!=joueur :
        
            for j in range(j,8):
                #print([i,j])
                if(Colonne[j] != joueur and Colonne[j]!=0):
                    k=Colonne[j]
                else :
                    k=Colonne[j]
                    break
        if(k==joueur):
            Liste.append([0,-1])
    if tous==False and len(Liste)==1 :
        return Liste

    #OUEST 
    if coup[1]>=2 :
        i=coup[0]
        j=coup[1]-1
        k=0
        if Plateau[i][j]!=joueur :
        
            for j in range(j,-1,-1):
            #print([i,j])
                if(Colonne[j] != joueur and Colonne[j]!=0 ):
                    k=Colonne[j]
                else :
                    k=Colonne[j]
                    break
        if(k==joueur):
            Liste.append([0,1])
    if tous==False and len(Liste)==1 :
        return Liste

    
    #SUD  

    if coup[0]<6 :
        i=coup[0]+1
        j=coup[1]
        k=0
        if Plateau[i][j]!=joueur :
   
            for i in range(i,8):
            #print([i,j])
                if(Plateau[i][j] != 0 and Plateau[i][j] != joueur):
                    k=Plateau[i][coup[1]]
                else :
                    k=Plateau[i][coup[1]]
                    break
            if(k==joueur):
                Liste.append([-1,0])
    if tous==False and len(Liste)==1 :
        return Liste
    
    #NORD
  
    k=0
    if coup[0]>=2 :
        i=coup[0]-1
        j=coup[1]
        if Plateau[i][j]!=joueur :
            for i in range(i,-1,-1):
                #print([i,j])
                if(Plateau[i][j] != 0 and Plateau[i][j] != joueur):
                    k=Plateau[i][j]
                else :
                    k=Plateau[i][j]
                    break
        if(k==joueur):
            Liste.append([1,0])
    if tous==False and len(Liste)==1 :
        return Liste
    



    #SUD-EST
    
    if(coup[0]<6 and coup[1]<6):
        j=coup[1]+1
        i=coup[0]+1
        k=0
        if Plateau[i][j]!=joueur :
            for i in range(i,8):
                #print([i,j])
                if j>7 :
                    break
                elif Plateau[i][j] != 0 and Plateau[i][j] != joueur :
                    k=Plateau[i][j]
                    j=j+1
                else :
                    k=Plateau[i][j]
                    break
        if(k==joueur):
            Liste.append([-1,-1])
    if tous==False and len(Liste)==1 :
        return Liste




    #SUD-OUEST
     
    if(coup[0]<6 and coup[1]>=2):
        j=coup[1]-1
        i=coup[0]+1
    
        k=0
        if Plateau[i][j]!=joueur :
            for i in range(i,8) :
                if j<0 :
                    break
                #print([i,j])
                elif Plateau[i][j] != 0 and Plateau[i][j] != joueur :
                    k=Plateau[i][j]
                    j=j-1
                else :
                    k=Plateau[i][j]
                    break
        if(k==joueur):
            Liste.append([-1,1])
    if tous==False and len(Liste)==1 :
        return Liste




    #NORD-EST
   
    if(coup[0]>=2 and coup[1]<6):
        j=coup[1]+1
        i=coup[0]-1

        k=0
        if Plateau[i][j]!=joueur :
            for i in range(i,-1,-1) :
                if j>7 :
                    break
                #print([i,j])
                elif Plateau[i][j] != 0 and Plateau[i][j] != joueur :
                    k=Plateau[i][j]
                    j=j+1
                else :
                    k=Plateau[i][j]
                    break
        if(k==joueur):
            Liste.append([1,-1])
    if tous==False and len(Liste)==1 :
        return Liste



    #NORD-OUEST

    if(coup[0]>=2 and coup[1]>=2):
        j=coup[1]-1
        i=coup[0]-1

        k=0
        if Plateau[i][j]!=joueur :
            for i in range(i,-1,-1) :
                if j<0 :
                    break
                #print([i,j])
                elif Plateau[i][j] != 0 and Plateau[i][j] != joueur :
                    k=Plateau[i][j]
                    j=j-1
                else :
                    k=Plateau[i][j]
                    break
        if(k==joueur):
            Liste.append([1,1])
    if tous==False and len(Liste)==1 :
        return Liste
    return Liste

def coups(jeu):
    """jeu->List[coup]
        Return une liste de coups pour le jeu tels qu'ils concernent chacun une case vide qui touche une case ou un pion de l'adversaire est place
    """
    plateau=jeu[0]
    L=[]
    for i in range(8):
        for j in range(8):
            if plateau[i][j]==0 :
                coup=[i,j]
                L.append(coup)
    return L

def listeCoupsValides(jeu):
    """ jeu->List[coup]
        Retourne la liste des coups valides selon l'etat du jeu. Un coup est valide si:
             - La case est vide et touche une case ou un pion de l'adversaire est place (assure par la fonction coups)
             - La case permet d'encadrer au moins une liste de pions de l'adversaire
    """
    cp=coups(jeu)
    s=[x for x in cp if len(trouveEncadrements(jeu,x,False))>0]
    return s

def FinJeu(jeu):
    if len(listeCoupsValides(jeu))==0 :
        return True
    return False

def retournePion(jeu,coup):
    Plateau=jeu[0]
    for i in trouveEncadrements(jeu,coup,True):
        if i==[1,0]:
            j=coup[0]
            k=coup[1]
            Plateau[j][k]=jeu[1]
            j=coup[0]-1
            for j in range(j,-1,-1):
                if (Plateau[j][k] == jeu[1]):
                    break
                if(Plateau[j][k] != jeu[1] and Plateau[j][k]!=0 ):
                    Plateau[j][k]=jeu[1]

        if i==[-1,0]:
            j=coup[0]
            k=coup[1]
            Plateau[j][k]=jeu[1]
            j=coup[0]+1
            for j in range(j,8):
                if (Plateau[j][k] == jeu[1]):
                    break
                if(Plateau[j][k] != jeu[1] and Plateau[j][k]!=0 ):
                    Plateau[j][k]=jeu[1]

        if i==[0,1]:
            j=coup[0]
            k=coup[1]
            Plateau[j][k]=jeu[1]
            k=coup[1]-1
            for k in range(k,-1,-1):
                if (Plateau[j][k] == jeu[1]):
                    break
                if(Plateau[j][k] != jeu[1] and Plateau[j][k]!=0 ):
                    Plateau[j][k]=jeu[1]


        if i==[0,-1]:
            j=coup[0]
            k=coup[1]
            Plateau[j][k]=jeu[1]
            k=coup[1]+1
            for k in range(k,8):
                if (Plateau[j][k] == jeu[1]):
                    break
                if(Plateau[j][k] != jeu[1] and Plateau[j][k]!=0 ):
                    Plateau[j][k]=jeu[1]


        if i==[1,1]:
            j=coup[0]
            k=coup[1]
            Plateau[j][k]=jeu[1]
            k=coup[1]-1
            j=coup[0]-1
            for k in range(k,-1,-1):
                if (Plateau[j][k] == jeu[1] or Plateau[j][k]==0 ):
                    break
                if j>7 or j<0 :
                    break
                #print([j,k])
                if(Plateau[j][k] != jeu[1] and Plateau[j][k]!=0 ):
                    Plateau[j][k]=jeu[1]
                j=j-1

        if i==[1,-1]:
            j=coup[0]
            k=coup[1]
            Plateau[j][k]=jeu[1]
            k=coup[1]+1
            j=coup[0]-1
            for k in range(k,8):
                if (Plateau[j][k] == jeu[1]):
                    break
                if j>7 or j<0 :
                    break
                #print([j,k])
                if(Plateau[j][k] != jeu[1] and Plateau[j][k]!=0 ):
                    Plateau[j][k]=jeu[1]
                j=j-1

        if i==[-1,1]:
            j=coup[0]
            k=coup[1]
            Plateau[j][k]=jeu[1]
            k=coup[1]-1
            j=coup[0]+1
            for k in range(k,-1,-1):
                if (Plateau[j][k] == jeu[1]):
                    break
                if j>7 or j<0 :
                    break
                #print([j,k])
                if(Plateau[j][k] != jeu[1] and Plateau[j][k]!=0 ):
                    Plateau[j][k]=jeu[1]
                j=j+1
        
        if i==[-1,-1]:
            j=coup[0]
            k=coup[1]
            Plateau[j][k]=jeu[1]
            k=coup[1]+1
            j=coup[0]+1
            for k in range(k,8):
                #print(Plateau[j][k])
                #print([j,k])
                if (Plateau[j][k] == jeu[1]):
                    break
                if j>7 or j<0 :
                    break
                if(Plateau[j][k] != jeu[1] and Plateau[j][k]!=0 ):

                    Plateau[j][k]=jeu[1]
                j=j+1

def Score(jeu):
    score=[0,0]
    Plateau=jeu[0]
    for i in range(8):
        for j in range(8):
            if Plateau[i][j]==1 :
                score[0]=score[0]+1
            elif Plateau[i][j]==2 :
                score[1]=score[1]+1
    jeu[4]=score
    return score

def finalise_partie(jeu):
    return 0