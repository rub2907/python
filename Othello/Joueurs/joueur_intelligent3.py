#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
global hor
hor=2
def evaluation(jeu):
    score=game.getScores(jeu)
    return (score[moi-1]-score[(moi%2+1)-1])

def estimation (jeu,coup,horizon,alpha,beta) :
    jeu1=game.getCopieJeu(jeu)
    game.joueCoup(jeu1,coup)
    #print(horizon)
    if game.finJeu(jeu1):
        if game.getGagnant(jeu1)==moi:
            return 1000
        else : 
            return -1000

    elif (horizon==hor) :
        #print("ici",res)
        return evaluation(jeu1)
    else:
        ListCV=game.getCoupsValides(jeu1)
        if jeu1[1]==moi:
            s=-100000
            for i in ListCV :
                s=max(s,estimation(jeu1,i,horizon+1,alpha,beta))
                if s>=beta :
                    return beta+1
                else :
                    alpha=max(alpha,s)
            return s

        else:
            s=100000
            for i in ListCV :
                s=min(s,estimation(jeu1,i,horizon+1,alpha,beta))
                if s<=alpha :
                    return alpha-1
                else :
                    beta=min(beta,s)
            return s
            

#dans un noeud min, on met à jour beta et on coupe l'evaluation en fonction de alpha (si une valeur des enfants est inférieure à alpha)
#dans un noeud max, on met à jour alpha et on  coupe l'evaluation en fonction de beta (si une valeur des enfants est supérieure à beta) 
        
def decision(jeu,ListCV):
    plateau=jeu[0]
    score=jeu[4]
    #joueur=jeu[1]
    res=[]
    alpha=-100000
 
    liste1=[[0,1],[0,2],[0,3],[0,4],[0,5],[0,6],[0,7]]
   # liste2=[[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0]]
   # liste3=[[1,1],[1,2],[1,3],[1,4],[1,5],[1,6],[1,7]]
  #  liste4=[[1,1],[2,1],[3,1],[4,1],[5,1],[6,1],[7,1]]
    for i in ListCV :
            if i in liste1:
                j=0
                while(j<i[1]):
                    if(plateau[0][j]==moi):
                        j=j+1
                    else:
                        break
                return i
    """for i in ListCV :
            if i in liste2:
                j=0
                while(j<i[0]):
                    if(plateau[j][0]==moi):
                        j=j+1
                    else:
                        break
                return i
    for i in ListCV :
            if i in liste3:
                j=0
                while(j<i[1]):
                    if(plateau[1][j]==moi and plateau[0][j]==moi):
                        j=j+1
                    else:
                        break
                return i
    for i in ListCV :
            if i in liste4:
                j=0
                while(j<i[0]):
                    if(plateau[j][1]==moi and plateau[j][0]==moi):
                        j=j+1
                    else:
                        break
                return i
   # liste5=[[1,1],[1,6],[6,1],[6,6]]"""
    b=False
    listeN=[[0,1],[1,0],[1,1],[0,6],[1,6],[1,7],[6,0],[6,1],[7,1],[7,6],[6,6],[6,7]]
    for i in ListCV :
        if i in [[0,0],[7,7],[0,7],[7,0]] :
            return i
        elif i not in listeN :
            a=estimation(jeu,i,1,alpha,100000)
            if a>alpha :
                alpha=a
                res=i
                b=True
    if b==False :
       
        for i in ListCV :
            a=estimation(jeu,i,1,alpha,100000)
            if a>alpha :
                alpha=a
                res=i 

    return res



""" for i in ListCV :
            if i not in liste5:
                a=estimation(jeu,i,1,alpha,100000)
                if a>alpha :
                    alpha=a
                    res=i 
        return res"""



def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    liste=game.getCoupsValides(jeu)
    global moi
    moi=jeu[1]
    #game.affiche(jeu)
    return decision(jeu,liste)