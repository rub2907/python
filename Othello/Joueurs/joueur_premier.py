#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    joueur=jeu[1]
    liste=game.getCoupsValides(jeu)
    if liste==[] :
    	return []
    coup=liste[0]
    return coup