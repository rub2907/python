#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
from random import *

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """

    joueur=jeu[1]
    liste=game.getCoupsValides(jeu)
    #print(liste)
    if liste==[] :
    	game.finJeu(jeu)
    	return []
    taille=len(liste)
    c=randint(0,taille-1)
    #print(c)
    #game.affiche(jeu)
    return liste[c]