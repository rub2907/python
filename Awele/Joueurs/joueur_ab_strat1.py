#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game

global hor
hor=3

def evaluation (jeu) :
    #finalise_partie(jeu)
    score=game.getScores(jeu)

    score[moi-1]+=eval_adv(jeu)
    score[moi-1]+=eval_moi(jeu)
    return score[moi-1]-(score[(moi%2+1)-1])

def eval_adv (jeu):
    nb=0
    plateau=jeu[0]
    for i in range(0,6):
        if plateau[(moi%2+1)-1][i] == 1 or  plateau[(moi%2+1)-1][i] == 2 :
            nb+=1
    return nb

def eval_moi (jeu):
    nb=0
    plateau=jeu[0]
    for i in range(0,6):
        if plateau[moi-1][i] == 1 or  plateau[moi-1][i] == 2 :
            nb-=1
    return nb

def estimation (jeu,coup,horizon,alpha,beta) :
    jeu1=game.getCopieJeu(jeu)
    game.joueCoup(jeu1,coup)
    #print(horizon)
    if game.finJeu(jeu1):
        if game.getGagnant(jeu1)==moi:
            return 1000
        else : 
            return -1000

    elif (horizon==hor) :
        #print("ici",res)
        return evaluation(jeu1)
    else:
        ListCV=game.getCoupsValides(jeu1)
        if jeu1[1]==moi:
            s=-100000
            for i in ListCV :
                s=max(s,estimation(jeu1,i,horizon+1,alpha,beta))
                if s>=beta :
                    return beta+1
                else :
                    alpha=max(alpha,s)
            return s

        else:
            s=100000
            for i in ListCV :
                s=min(s,estimation(jeu1,i,horizon+1,alpha,beta))
                if s<=alpha :
                    return alpha-1
                else :
                    beta=min(beta,s)
            return s
            


def decision(jeu,ListCV):
    
    score=jeu[4]
    #joueur=jeu[1]
    res=[]
    alpha=-100000
    for i in ListCV :
        a=estimation(jeu,i,1,alpha,100000)
        #print(a)
        #print(jeu[1])
        if a>alpha :
            alpha=a
            res=i
    return res






def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    liste=game.getCoupsValides(jeu)
    global moi
    moi=jeu[1]
    """if len(jeu[3]) in [0]:
        return liste[5]
    elif len(jeu[3]) in [2]:
        return liste[1]
    elif len(jeu[3]) in [4]:
        return liste[3]
    else :"""
    #game.affiche(jeu)
    return decision(jeu,liste)
