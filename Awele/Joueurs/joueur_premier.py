#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    joueur=jeu[1]
    liste=game.getCoupsValides(jeu)

    if liste==[] :
    	return []
    if joueur==1:
    	coup=liste[0]
    else :
    	coup=liste[-1]
    return coup