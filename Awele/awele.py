#!/usr/bin/env python
# -*- coding: utf-8 -*-

def InitialiseJeu():
	Plateau=[[4,4,4,4,4,4],[4,4,4,4,4,4]]
	List_Score=[0,0]
	joueur=1
	List_coupsV=None
	List_coups=[]
	Jeu=[Plateau,joueur,List_coupsV,List_coups,List_Score]
	return Jeu

jeu=InitialiseJeu()

def tiret():
    print("\n----------------------------------------------------------")

def FinJeu(jeu) :
	
	if (listeCoupsValides(jeu)==[]):
		return True
	return False
"""Fin de partie. La partie s'arrête lorsqu'un joueur ne peut plus jouer, n'ayant plus de graine. 
Chaque joueur comptabilise ses graines restantes dans son score. 
#Le jeu s'arrête aussi à la première position déjà rencontrée au cours de la partie (jeu cyclique)."""


def listeCoupsValides(jeu):
	joueur=jeu[1]
	Plateau=jeu[0]
	coup=Plateau[joueur-1]
	jeu[2]=[]
	ListeAff=[0,0,0,0,0,0]
	if (joueur==1) and (Plateau[1]==ListeAff):
		for j in range(6):
			if coup[j]>(j) :
				liste=[joueur-1,j]
				jeu[2].append(liste)
	elif (joueur==2) and (Plateau[0]==ListeAff):
		for j in range(6):
			if coup[j]>=(6-j) :
				liste=[joueur-1,j]
				jeu[2].append(liste)
	else :
		for i in range(len(coup)):
			if coup[i] !=0:
				liste=[joueur-1,i]
				jeu[2].append(liste)
	return jeu[2]

def nextCase(jeu,case,inv=True):
    """jeu*Pair[nat nat]->Pair[nat nat]
        Retourne la prochaine case sur le plateau, dans le sens inverse des aiguilles d'une montre si inv est vrai, 
        dans le sens des aiguilles d'une montre sinon
    """
    Plateau=jeu[0]
    joueur=jeu[1]
    if(inv):
    	if (case[0]==0) :
    		if(case[1]>0):
    			case[1]=case[1]-1
    			return case
    		else :
        		if(joueur==1):
        			case[0]=1
        			case[1]=0
        			return case
        		elif(joueur==1 and case[0]==1):
        			case[0]=0
        			case[1]=5
        			return case
        		if(joueur==2 and case[0]==0):
        			case[0]=1
        			case[1]=0
        			return case
        		elif(joueur==2 and case[0]==1):
        			case[0]=0
        			case[1]=5
        			return case

    	else :
    		if(case[1]<5):
    			case[1]=case[1]+1
    			return case
    		
    		else :
        		if(joueur==1 and case[0]==0):
        			case[0]=1
        			case[1]=0
        			return case
        		elif(joueur==1 and case[0]==1):
        			case[0]=0
        			case[1]=5
        			return case
        		if(joueur==2 and case[0]==0):
        			case[0]=1
        			case[1]=0
        			return case
        		elif(joueur==2 and case[0]==1):
        			case[0]=0
        			case[1]=5
        			return case	
    else:
    	if case[0]==joueur-1 :
    		return case
    	if (joueur==1):
    		if(case[1]>0):
    			case[1]=case[1]-1
    			return case
    	else :
    		if (case[1]<5):
    			case[1]=case[1]+1
    			return case
        
    return case


def peutManger(jeu,case):
    """jeu * Pair[nat nat] -> bool
        Retourne vrai si on peut manger le contenu de la case:
            - c'est une case appartenant a l'adversaire du joueur courant
            - La case contient 2 ou 3 graines
    """
    plateau=jeu[0]
    colonne=plateau[case[0]]
    joueur=jeu[1]
    if(joueur!= case[0]+1):
    	return (colonne[case[1]]==2 or colonne[case[1]]==3)
    else:
    	return False

def ajoute_graine(jeu,case):
	plateau=jeu[0]
	colonne=plateau[case[0]]
	colonne[case[1]]=colonne[case[1]]+1


def distribue(jeu,cas,nb):
    """ jeu * Pair[nat nat] * nat -> void
    Egraine nb graines (dans le sens inverse des aiguilles d'une montre) en partant de la case suivant la case suivant celle pointee par cellule,
    puis mange ce qu'on a le droit de manger
    Pseudo-code :
    - Distribution des graines dans le sens inverse des aiguilles d'une montre en evitant la case de depart (si nb>=12, on nourrit plusieurs fois les memes cases)
    - Parcours du plateau dans le sens inverse tant qu'on peut manger des graines
    Note: on egraine pas dans la case de depart si on a fait un tour
    Note2: On ne mange rien si le coup affame l'adversaire
    """
    plateau=jeu[0]
    sc=0
    sc1=0
    score=jeu[4]
    if (cas==[]):
    	#print("fin_jeu")
    	for i in plateau[0] :
    		sc=sc+i
    	#print("sc",sc)
    	for i in plateau[1] :
    		sc1=sc1+1
    	#print("sc1",sc1)
    	if sc1>0 :
    		score[1]+sc1
    	elif sc>0 :
    		score[0]+sc
    else :
    	case=[0,0]
    	case[0]=cas[0]
    	case[1]=cas[1]
    	caseD=case[0]
    	caseD1=case[1]
    	joueur=jeu[1]
    	colonne=plateau[case[0]]
    	colonne[case[1]]=0
    	nbManger=0
    	liste=[]
    	case=nextCase(jeu,case,True)
    	while(nb>1):
    		if(case!=[caseD,caseD1]):
    				ajoute_graine(jeu,case)
    				nb=nb-1
    				case=nextCase(jeu,case,True)
    			
    		else :
    			case=nextCase(jeu,case,True)
    	if (nb==1):
    		ajoute_graine(jeu,case)
    		p=[]
    		Res=plateau[case[0]]
    		liste=[0,0,0,0,0,0]
    		while (peutManger(jeu,case)):
    			p=plateau[case[0]]
    			nbManger=nbManger+p[case[1]]
    			liste[case[1]]=p[case[1]]
    			p[case[1]]=0
    			case=nextCase(jeu,case,False)
    		if (p==[0,0,0,0,0,0]):
    			nbManger=0
    			plateau[case[0]]=liste
    	#print("manger:",nbManger)
    
    	if joueur==1 :
    		score[0]=score[0]+nbManger
    	else :
    		score[1]=score[1]+nbManger
    	#print("score :", score)

def finalise_partie(jeu):
    if (FinJeu(jeu)):
        plateau=jeu[0]
        sc=0
        sc1=0
        score=jeu[4]
        for i in plateau[0] :
            sc=sc+i
        for i in plateau[1] :
            sc1=sc1+i
        if sc1>0 :
            score[1]=score[1]+sc1
        elif sc>0 :
            score[0]=score[0]+sc






























		





